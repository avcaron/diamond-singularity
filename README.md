# diamond-singularity

Git repository for diamond singularity building script, configuration and how-to.

***

Requirements
------------

*All components needs to be named as bellow in order for the singularity script to work. They also need to be placed at the root containing the singularity .def file.*

- **intel_parallel_studio.tgz** : an *intel parallel studio 2019, update 4* installation package for linux, as downloaded from intel website

- **intel_ps_license.lic** : a license file for *intel parallel studio 2019, update 4*

- **intel_cfg_pub_key** : an *intel runtime* public key for *apt*

- **diamond_package.tar.gz** : a *diamond* installation package containing the cloned diamond repository at its root

- Singularity installed on the machine (you can get it with *apt-get install singularity*)

***

Installation
------------

Run the following command to create the image

    sudo singularity build diamond_singularity.simg singularity_diamond.def

A test to ensure diamond and its required libraries are well installed and available will be run after the image completion. To build the singularity without the test, run

    sudo singularity build --notest diamond_singularity.simg singularity_diamond.def

***

Usage
-----

The entry point to diamond on the singularity image is an app called *diamond*. Use the following command

    singularity run [args] --app diamond [singularity image] <diamond_args>
